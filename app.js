//app.js
App({
	// 接口地址
	api:{
		condition: "/forum?fetch_data=search_options",
		list: "/thread",
		detail: "/thread",
		posts: "/post",
		website: "/website",
		wechat: "/wechat",
        apiEndpointLocal: "http://api.yibada.com",
        apiEndpoint: "https://www.yibada.com/api/app/public/index.php"
	},
	// 获取用户信息
	getUserInfo: function(cb) {
		var app = this;
		if (this.globalData.userInfo) {
			typeof cb == "function" && cb(this.globalData.userInfo)
		} else {
			//调用登录接口
			wx.login({
				success: function(res) {
					var code = res.code;
					wx.getUserInfo({
						success: function(res) {
				            wx.request({
				                url: app.api.apiEndpoint+app.api.wechat,
				                method: 'POST',
				                data: {
				                	handler: 'login_with_code',
				                	code: code,
				                	appid: app.globalData.appId,
				                	user_info: res.userInfo,
				                	test_open_id: app.globalData.testOpenId ? app.globalData.testOpenId : null,
 				                },
				                header: {
				                    'Content-Type': 'application/json'
				                },
				                success: function( userData ) {
				                	app.globalData.user = {
				                		uid: userData.uid,
				                		username: userData.username
				                	},
				                	typeof cb == "function" && cb(userData);
				                },
				                fail:function(){
				                    console.log('error!!!!!!!!!!!!!!')
				                }
				            });
						}
					})
				}
			})
		}
	},
	detectWebsite: function(cb){
		var that = this;
		// wx.removeStorageSync('website');
		wx.getStorage({
		  key: 'website',
		  success: function(res){
			if(!res.data){
				that.detectGeoInfo(cb);
			} else {
				that.globalData.website = res.data;
				typeof cb == "function" && cb(res.data)
			}
		  },
		  fail: function(res) {
			that.detectGeoInfo(cb);
		  },
		})
	},
	detectGeoInfo: function(cb){
		var that = this;
		if (!this.globalData.geoInfo) {
			wx.getLocation({
				success: function(res) {
					that.globalData.geoInfo = res;
					that.detectCityByGeo(cb);
				},
				fail: function(res){
					wx.navigateTo({
						url: '../city/detect'
					});
				}
			});
		} else {
			that.detectCityByGeo(cb);
		}
	},
	detectCityByGeo: function(cb){
		var that = this;
		wx.showToast({
			title: '正在获取位置信息',
			icon: 'loading',
		})
		wx.request( {
            url:that.api.apiEndpoint+that.api.website,
            data:{
                fetch_data: 'by_location',
                x: that.globalData.geoInfo.latitude,
                y: that.globalData.geoInfo.longitude
            },
            header: {
                'Content-Type': 'application/json'
            },
            success: function( res ) {
            	if(res.data && res.data.wid){
	                that.globalData.website = res.data;
	                wx.setStorage({
	                  key: 'website',
	                  data: res.data
	                });
					wx.hideToast();
					typeof cb == "function" && cb(res.data);
            	} else {
            		wx.showToast({
            			title: '无法定位，正在跳转到城市选择页面',
            			icon: 'loading',
            		})
            		wx.navigateTo({
    					url: '../city/detect'
    				});
            	}
            },
            fail:function(){
				wx.hideToast();
				wx.navigateTo({
					url: '../city/detect'
				});
            }
        })


	},
	setTitle: function(title){
		wx.setNavigationBarTitle({
			title: title
  		});
		wx.hideNavigationBarLoading();
	},
    // 全局变量
	globalData: {
		appId: "touristappid",
		testOpenId: "testwechatopenid",
	}
})